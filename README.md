# tree-sitter-remind

[Remind][] grammar for [tree-sitter][].

[Remind]: https://dianne.skoll.ca/projects/remind/
[tree-sitter]: https://github.com/tree-sitter/tree-sitter

## Progress tracking

- [ ] Stop parsing at `__EOF__` and comments.
- [ ] `INC[LUDE]`, `INCLUDECMD` and `DO`
- [ ] `DEBUG`
- [ ] `DUMP[VARS]`
- [ ] `ERRMSG`, `EXIT` `FLUSH`
- [ ] `BAN[NER]`
- [ ] `RUN {ON, OFF}`
- Expressions
  - [ ] INT
  - [ ] STRING
  - [ ] TIME
  - [ ] DATE
  - [ ] DATETIME
  - [ ] OPERATORS `! - * / % + - < <= > >= == != && ||`
  - [ ] VARIABLES, SYSTEM VARIABLES and FUNCTIONS
  - [ ] `[F][UN]SET` and `PRESERVE`

- [ ] EXPRESSION PASTING
- [ ] `IF[TRIG]`, `ELSE` and `ENDIF`
- [ ] `{PUSH, CLEAR, POP}[-OMIT-CONTEXT]`
- [ ] `OMIT`
  - [ ] `OMIT DUMP`
  - [ ] `OMIT weekday [weekday...]`
  - [ ] `OMIT [day] month [year]`
  - [ ] `OMIT [day1] month1 [year1] THROUGH [day2] month2 [year2]`
  - [ ] `OMIT` with delta
  - [ ] `OMIT` with `MSG` or `RUN`.
- [ ] Substitution filters `%{name}` and `%*{name}`
- [ ] `REM[IND]` or nothing.
